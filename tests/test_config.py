# -*- coding: utf-8 -*-
import httpretty
from os import unsetenv, environ, path, remove

from snippet.config import CommandLineConfig
from pybitbucket.bitbucket import Client


class TestConfig(object):

    def test_default_bitbucket_url(self):
        unsetenv('BITBUCKET_URL')
        # The default is production
        assert 'https://api.bitbucket.org' == CommandLineConfig.bitbucket_url()

    def test_override_bitbucket_url(self):
        override_url = 'https://staging.bitbucket.org/api'
        environ['BITBUCKET_URL'] = override_url
        # Override the default when the environment variable is set
        assert override_url == CommandLineConfig.bitbucket_url()

    def test_config_file(self):
        my_config = CommandLineConfig.config_file()
        head, my_config_file = path.split(my_config)
        assert 'bitbucket.json' == my_config_file
        head, my_config_dir = path.split(head)
        assert '.snippet' == my_config_dir

    def test_load_test_config_file(self):
        test_dir, current_file = path.split(path.abspath(__file__))
        project_dir, test_dir = path.split(test_dir)
        my_config_path = CommandLineConfig.config_file(project_dir, test_dir)
        head, my_config_file = path.split(my_config_path)
        assert 'bitbucket.json' == my_config_file
        head, my_config_dir = path.split(head)
        assert 'tests' == my_config_dir
        my_config = open(my_config_path)
        assert not my_config.closed
        my_config.close()

    def test_load_test_config_data(self):
        test_dir, current_file = path.split(path.abspath(__file__))
        project_dir, test_dir = path.split(test_dir)
        my_config_path = CommandLineConfig.config_file(project_dir, test_dir)
        my_bitbucket_url = 'https://staging.bitbucket.org/api'
        my_config = CommandLineConfig.load_config(
            config_file=my_config_path,
            bitbucket_url=my_bitbucket_url)
        assert my_bitbucket_url == my_config.get('server_base_uri')
        assert 'pybitbucket-staging@mailinator.com' == \
            my_config.get('client_email')
        assert 'pybitbucket-staging' == my_config.get('username')
        assert 'secret' == my_config.get('password')

    def test_config_construction(self):
        test_dir, current_file = path.split(path.abspath(__file__))
        project_dir, test_dir = path.split(test_dir)
        my_config_path = CommandLineConfig.config_file(project_dir, test_dir)
        my_bitbucket_url = 'https://staging.bitbucket.org/api'
        my_config = CommandLineConfig(
            config_file=my_config_path,
            bitbucket_url=my_bitbucket_url)
        assert my_bitbucket_url == my_config.server_base_uri
        assert 'pybitbucket-staging@mailinator.com' == my_config.client_email
        assert 'pybitbucket-staging' == my_config.username
        assert 'secret' == my_config.password

    def test_save_for_new_config_data(self):
        test_dir, current_file = path.split(path.abspath(__file__))
        project_dir, test_dir = path.split(test_dir)
        my_bitbucket_url = 'https://staging.bitbucket.org/api'
        out_config_path = CommandLineConfig.config_file(
            project_dir,
            test_dir,
            'temp_test_save_config_data.json')
        CommandLineConfig.save_config(
            username='pybitbucket-staging',
            password='secret',
            email='pybitbucket-staging@mailinator.com',
            config_file=out_config_path,
            bitbucket_url=my_bitbucket_url)
        my_config = CommandLineConfig(
            config_file=out_config_path,
            bitbucket_url=my_bitbucket_url)
        assert my_bitbucket_url == my_config.server_base_uri
        assert 'pybitbucket-staging@mailinator.com' == my_config.client_email
        assert 'pybitbucket-staging' == my_config.username
        assert 'secret' == my_config.password
        remove(out_config_path)

    @httpretty.activate
    def test_client_construction(self):
        # Set the Bitbucket URL via an environment variable.
        test_url = 'https://api.bitbucket.org'
        environ['BITBUCKET_URL'] = test_url
        # Use snippet's file-based class for configuration.
        client = Client(CommandLineConfig())
        # Test the variable was picked up.
        assert test_url == client.config.server_base_uri
        url = client.get_bitbucket_url() + '/1.0/user'
        httpretty.register_uri(httpretty.GET, url)
        response = client.session.get(url)
        assert 200 == response.status_code
